echo "Running the input preparation for the turbulent simulation"
#printing the message above
n=1
#setting the variable equal to 1
for Re in 50000 100000 500000 1000000
do
	m=1
	for zMesh in 512 1024
	do
#for different value of Re there is 2 zMesh
		o=1
		for yMesh in 128 256
		do
			d=1
			for xMesh in 256 512
			do
#and for each zmech 2 ymeshs
			mkdir Raghid${n}_${m}_${o}_${d}
#creating folder Raghid for diff values of n m o, n for Re m for zmesh and y for ymesh
			cd originalF
			sed -e "s/rrrrrrr/${Re}/" -e "s/xxxx/${zMesh}/" -e "s/yyy/${yMesh}/" -e "s/zzz/${xMesh}/" inputOrig > input.dat
			echo " setting in inputOrig Re=${Re}, zmesh=${zMesh} and ymesh=${yMesh} and xmesh=${xMesh} and saving the results in Raghid${n}_${m}_${o}_${d}"
#entering the originalF folder and setting the value of Re, zmesh and ymesh on inputOrig and save it to input.dat
			cp input.dat ../Raghid${n}_${m}_${o}_${d}
#copy each input.dat to its corresponding folder: Raghid$Re_$zmesh_$ymesh
			cd ../
				d=$(($d + 1))
			done
			o=$(( $o + 1 ))
		done
		m=$(( $m + 1 ))
	done
	n=$(( $n + 1 ))
#incrementing the three variables
done
echo "input file preparation is done"
#printing the message above when the loop is done
